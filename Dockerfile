FROM node:18-alpine as build-stage
WORKDIR /app

COPY ./package.json /app/
COPY ./package-lock.json /app/
RUN NODE_ENV=development npm ci
COPY . /app/

RUN npm run ng build

FROM nginx:alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html
COPY --from=build-stage /app/dist/* ./

EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]
